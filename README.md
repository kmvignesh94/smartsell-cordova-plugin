
# Smartsell Cordova Plugin

## Installation
```sh
$ cd CORDOVA_PROJECT
$ cordova plugin add https://gitlab.com/enparadigm_public/smartsell-cordova-test-plugin
```
**Note:** The plugin which we are using is present in private repository. So you need to add following lines in your gradle.properties file.
```
artifactory_repo_smartsell_release_url=http://artifactory.enparadigm.com/artifactory/smartsell_release
artifactory_username=USERNAME_GIVEN_BY_US
artifactory_password=PASSWORD_GIVEN_BY_US
```


### Handling PushNotifiction
Our Library can handle the push notification for you. To make it happen we need to add following in your ``onMessageReceived`` function of ``FirebaseMessagingService``
```kotlin
@Override  
public void onMessageReceived(RemoteMessage remoteMessage) {  
  super.onMessageReceived(remoteMessage);  
  if (SmartSell.isSmartSellNotification(remoteMessage)) {
    SmartSell.handleFcmNotification(this, remoteMessage, MainActivity.class); // MainActivity will be opened while closing our sdk
  }
}
```

Also make sure to initialize our library in your Application class. If we are not calling init in your Application class, then the app will crash on receving push Notification.
```kotlin
// Import these packages  
import com.enparadigm.smartsell.SmartSell;  
import com.enparadigm.smartsell.SmartSellConfig;


@Override  
public void onCreate() {  
  //Add this code in your Application class
  //-------START----------  
  SmartSellPluginConfig pluginConfig = SmartSellPluginConfig.getInstance(this);  
  if (pluginConfig.base_url != null && pluginConfig.base_url.length() > 10) {  
  SmartSell.isUserInitialised(getApplicationContext(), pluginConfig.mobile, new InitCallback() {  
        @Override  
        public void onSuccess() {  }  
        @Override  
        public void onError(Throwable throwable) {  }  
    });  
  }
  //-------END-----------
  super.onCreate();  
}
```

## Plugin functions
There are 5 functions in the plugin,
 1. **isUserInitialised** will validate SDK has all required data. If the data present then it will open the SDK.
 2. **initialise** will initialise the plugin with given data.
 
**Note:** You can skip calling **isUserInitialised** for the first time, as the SDK will not have required data untill we do **initialise**. Next time onword you can call **isUserInitialised** to open the SDK. 

### Sample code to call plugin function
```javascript
// We don't need to start the SDK. if initialisation is succes then it will open the SDK.
// Make sure to pass same mobile number for isUserInitialised and initialise functions
function isUserInitialised() {
    var plugin = new SmartSellPlugin();
    // Here 9999999999 is mobile number
    plugin.isUserInitialised("9999999999",function (msg) {
    console.log(msg) // Success Callback. 
    },
        function (err) {
            console.log(err); 
            /*First time we get this error , because we need to init the SDK atlease once. 
            If you get error then, your app need to get new AccessToken and RefreshToken and pass it to the SDK. 
            To pass new tokens use initialise Function. 
            Next time onwords if you call this function it should return success and open the SDK. */
        });
}

/* Make sure to pass the parameter in the following order "API_URL_FOR_PLUGIN,MOBILE_NUMBER,NAME,EMAIL,DESIGNATION,LANGUAGE_SHORT_FORM,USER_TYPE,ACCESS_TOKEN,REFRESH_TOKEN" for this function
  API_URL_FOR_PLUGIN - URL to which the plugin should connect
  LANGUAGE_SHORT_FORM[send "en" for English] - plugin langugae.  eg: If you want plugin in Hindi language then send "hi"
*/
function initialise() {
  var smartSellPlugin = new SmartSellPlugin();
  smartSellPlugin.initialise("http://dev.enparadigm.com/starhealth_dev_api/public/index.php/v1/","9999999999","UsersName","email@mail.com","Developer","en","1","access token", "refresh token",function (msg) {
              console.log(msg);
        },
        function (err) {
              console.log(err);
        });
}
```